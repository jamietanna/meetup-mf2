package me.jvt.meetup.mf2.converter.rest;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HEvent.Properties;
import me.jvt.meetup.mf2.model.HEvent.Properties.Author;
import me.jvt.meetup.mf2.model.HEvent.Properties.Author.AuthorProperties;
import me.jvt.meetup.mf2.model.HEvent.Properties.Description;
import me.jvt.meetup.mf2.model.rest.MeetupEventResponse;
import org.jsoup.Jsoup;

public class MeetupEventConverter {
  private static final String MEETUP_URL = "https://www.meetup.com";
  private static final long ONLINE_EVENT_VENUE_ID = 26906060;

  private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

  public HEvent convert(MeetupEventResponse meetupEventResponse) {
    HEvent hEvent = new HEvent();
    hEvent.type = singletonArray("h-event");
    hEvent.properties = new Properties();

    // determine start datetime
    OffsetDateTime start =
        Instant.ofEpochMilli(meetupEventResponse.time)
            .atOffset(ZoneOffset.ofTotalSeconds((int) (meetupEventResponse.utcOffset / 1000)));
    OffsetDateTime end = start.plus(meetupEventResponse.duration, ChronoUnit.MILLIS);

    hEvent.properties.start = singletonArray(dateTimeFormatter.format(start));
    hEvent.properties.end = singletonArray(dateTimeFormatter.format(end));

    hEvent.properties.name = singletonArray(meetupEventResponse.name);

    //    hEvent.properties.description = singletonArray();
    if (null != meetupEventResponse.description) {
      hEvent.properties.description = new Description[1];
      hEvent.properties.description[0] = new Description();
      hEvent.properties.description[0].html = meetupEventResponse.description;
      hEvent.properties.description[0].value = Jsoup.parse(meetupEventResponse.description).text();
    }

    hEvent.properties.url = singletonArray(meetupEventResponse.link);

    // location
    if (null != meetupEventResponse.venue) {
      if (ONLINE_EVENT_VENUE_ID == meetupEventResponse.venue.id) {
        hEvent.properties.location = singletonArray("Online");
      } else {
        String streetAddress =
            String.format(
                "%s, %s", meetupEventResponse.venue.name, meetupEventResponse.venue.address1);
        String locality = meetupEventResponse.venue.city;
        String countryName = meetupEventResponse.venue.localized_country_name;
        String location = String.format("%s, %s, %s", streetAddress, locality, countryName);
        hEvent.properties.location = singletonArray(location);
      }
    }

    // author info
    Author author = new Author();
    author.type = singletonArray("h-card");
    author.properties = new AuthorProperties();
    author.value = meetupEventResponse.group.name;
    author.properties.name = singletonArray(meetupEventResponse.group.name);
    author.properties.photo = singletonArray(meetupEventResponse.group.keyPhoto.photoLink);
    author.properties.url =
        singletonArray(String.format("%s/%s", MEETUP_URL, meetupEventResponse.group.urlname));
    hEvent.properties.author = Collections.singletonList(author);

    // set publish date and updated date to work better with feed rendering
    hEvent.properties.published =
        singletonArray(
            dateTimeFormatter.format(
                Instant.ofEpochMilli(meetupEventResponse.created).atOffset(ZoneOffset.UTC)));
    hEvent.properties.updated =
        singletonArray(
            dateTimeFormatter.format(
                Instant.ofEpochMilli(meetupEventResponse.updated).atOffset(ZoneOffset.UTC)));

    return hEvent;
  }

  private String[] singletonArray(String value) {
    String[] strings = new String[1];
    strings[0] = value;
    return strings;
  }
}
