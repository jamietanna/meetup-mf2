package me.jvt.meetup.mf2.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MeetupGroupResponse {
  public String name;
  public String urlname;

  @JsonProperty("key_photo")
  public KeyPhoto keyPhoto;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class KeyPhoto {
    @JsonProperty("photo_link")
    public String photoLink;
  }
}
