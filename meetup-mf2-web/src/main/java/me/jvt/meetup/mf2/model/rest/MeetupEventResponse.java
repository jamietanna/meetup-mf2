package me.jvt.meetup.mf2.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MeetupEventResponse {

  public long created;
  public long duration;
  public String id;
  public String name;
  public long time;
  public long updated;

  @JsonProperty("utc_offset")
  public long utcOffset;

  public MeetupGroupResponse group;
  public Venue venue;
  public String link;
  public String description;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Venue {

    @JsonProperty("name")
    public String name;

    @JsonProperty("address_1")
    public String address1;

    public String city;
    public String country;
    public String localized_country_name;
    public long id;
  }
}
