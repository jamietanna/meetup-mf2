package me.jvt.meetup.mf2.exception;

import me.jvt.meetup.mf2.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(MeetupApiException.class)
  public final ResponseEntity<ErrorResponse> handleMeetupApiException(
      MeetupApiException meetupApiException, WebRequest req) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.errorMessage = meetupApiException.getMessage();
    errorResponse.errors = meetupApiException.getMeetupErrorResponse().errors;
    return new ResponseEntity<>(errorResponse, meetupApiException.getMeetupErrorStatusCode());
  }
}
