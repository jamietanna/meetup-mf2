package me.jvt.meetup.mf2.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HFeed {

  public List<HEvent> children;
  public String[] type = {"h-feed"};
  public Properties properties;

  public static class Properties {
    public String[] name;
    public String[] url;
  }
}
