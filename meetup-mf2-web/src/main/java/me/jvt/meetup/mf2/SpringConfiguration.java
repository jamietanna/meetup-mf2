package me.jvt.meetup.mf2;

import me.jvt.meetup.mf2.converter.graphql.MeetupEventConverter;
import me.jvt.meetup.mf2.external.meetup.graphql.MeetupClient;
import me.jvt.meetup.mf2.restassured.RequestSpecificationFactory;
import me.jvt.meetup.mf2.service.EventService;
import me.jvt.meetup.mf2.service.GraphqlEventService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class SpringConfiguration {

  @Bean
  public RequestSpecificationFactory requestSpecificationFactory() {
    return new RequestSpecificationFactory();
  }

  @Bean
  public MeetupClient meetupClient(RequestSpecificationFactory requestSpecificationFactory) {
    return new MeetupClient(requestSpecificationFactory);
  }

  @Bean
  public MeetupEventConverter meetupEventConverter() {
    return new MeetupEventConverter();
  }

  @Bean
  public EventService eventService(
      MeetupClient meetupClient, MeetupEventConverter meetupEventConverter) {
    return new GraphqlEventService(meetupClient, meetupEventConverter);
  }
}
