package me.jvt.meetup.mf2.restassured;

import static io.restassured.RestAssured.given;

import io.restassured.specification.RequestSpecification;

public class RequestSpecificationFactory {

  public RequestSpecification newRequestSpecification() {
    return given();
  }
}
