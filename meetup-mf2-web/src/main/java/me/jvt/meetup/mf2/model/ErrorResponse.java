package me.jvt.meetup.mf2.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.jvt.meetup.mf2.model.rest.MeetupErrorResponse;

public class ErrorResponse {

  @JsonProperty("error_message")
  public String errorMessage;

  public MeetupErrorResponse.Error[] errors;
}
