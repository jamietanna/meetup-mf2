package me.jvt.meetup.mf2.exception;

import me.jvt.meetup.mf2.model.rest.MeetupErrorResponse;
import org.springframework.http.HttpStatus;

public class MeetupApiException extends Exception {

  private final MeetupErrorResponse meetupErrorResponse;
  private final HttpStatus meetupErrorStatusCode;

  public MeetupApiException(
      String message, int meetupErrorStatusCode, MeetupErrorResponse meetupErrorResponse) {
    super(message);
    this.meetupErrorResponse = meetupErrorResponse;
    this.meetupErrorStatusCode = HttpStatus.valueOf(meetupErrorStatusCode);
  }

  public MeetupErrorResponse getMeetupErrorResponse() {
    return meetupErrorResponse;
  }

  public HttpStatus getMeetupErrorStatusCode() {
    return meetupErrorStatusCode;
  }
}
