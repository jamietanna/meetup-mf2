package me.jvt.meetup.mf2.external.meetup.rest;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.jvt.meetup.mf2.CacheConfiguration;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.model.rest.MeetupErrorResponse;
import me.jvt.meetup.mf2.model.rest.MeetupEventResponse;
import me.jvt.meetup.mf2.model.rest.MeetupGroupResponse;
import me.jvt.meetup.mf2.restassured.RequestSpecificationFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

public class MeetupClient {

  private final RequestSpecificationFactory requestSpecificationFactory;

  public MeetupClient(RequestSpecificationFactory requestSpecificationFactory) {
    this.requestSpecificationFactory = requestSpecificationFactory;
  }

  @Cacheable(CacheConfiguration.EVENTS_CACHE)
  public MeetupEventResponse retrieveEvent(String urlname, String id) throws MeetupApiException {
    RequestSpecification requestSpecification =
        requestSpecificationFactory.newRequestSpecification();

    Response meetupApiResponse =
        requestSpecification
            .baseUri("https://api.meetup.com")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .queryParam("fields", "group_key_photo")
            .get(String.format("/%s/events/%s", urlname, id));

    if (HttpStatus.OK.value() != meetupApiResponse.getStatusCode()) {
      throw new MeetupApiException(
          "An error occurred when talking to the Meetup API",
          meetupApiResponse.getStatusCode(),
          meetupApiResponse.as(MeetupErrorResponse.class));
    }

    return meetupApiResponse.as(MeetupEventResponse.class);
  }

  @Cacheable(CacheConfiguration.EVENTS_CACHE)
  public List<MeetupEventResponse> retrieveEvents(String urlname) throws MeetupApiException {
    RequestSpecification requestSpecification =
        requestSpecificationFactory.newRequestSpecification();

    Response meetupApiResponse =
        requestSpecification
            .baseUri("https://api.meetup.com")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .queryParam("state", "upcoming")
            .queryParam("fields", "group_key_photo")
            .get(String.format("/%s/events", urlname));

    if (HttpStatus.OK.value() != meetupApiResponse.getStatusCode()) {
      throw new MeetupApiException(
          "An error occurred when talking to the Meetup API",
          meetupApiResponse.getStatusCode(),
          meetupApiResponse.as(MeetupErrorResponse.class));
    }

    MeetupEventResponse[] arr = meetupApiResponse.as(MeetupEventResponse[].class);
    return new ArrayList<>(Arrays.asList(arr));
  }

  @Cacheable(CacheConfiguration.GROUPS_CACHE)
  public MeetupGroupResponse retrieveGroup(String urlname) throws MeetupApiException {
    RequestSpecification requestSpecification =
        requestSpecificationFactory.newRequestSpecification();

    Response meetupApiResponse =
        requestSpecification
            .baseUri("https://api.meetup.com")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .get(String.format("/%s", urlname));

    if (HttpStatus.OK.value() != meetupApiResponse.getStatusCode()) {
      throw new MeetupApiException(
          "An error occurred when talking to the Meetup API",
          meetupApiResponse.getStatusCode(),
          meetupApiResponse.as(MeetupErrorResponse.class));
    }

    return meetupApiResponse.as(MeetupGroupResponse.class);
  }
}
