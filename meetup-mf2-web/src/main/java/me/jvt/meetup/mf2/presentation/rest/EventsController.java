package me.jvt.meetup.mf2.presentation.rest;

import java.util.ArrayList;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HFeed;
import me.jvt.meetup.mf2.model.MicroformatsResponseContainer;
import me.jvt.meetup.mf2.service.EventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventsController {

  private final EventService eventService;

  public EventsController(EventService eventService) {
    this.eventService = eventService;
  }

  @GetMapping(value = "/{urlname}/events/{id}", produces = "application/mf2+json")
  public ResponseEntity<MicroformatsResponseContainer> getEvent(
      @PathVariable("urlname") String urlname, @PathVariable("id") String id)
      throws MeetupApiException {
    MicroformatsResponseContainer<HEvent> responseContainer = new MicroformatsResponseContainer<>();
    responseContainer.items = new ArrayList<>();
    responseContainer.items.add(eventService.retrieveEvent(urlname, id));
    return new ResponseEntity<>(responseContainer, HttpStatus.OK);
  }

  @GetMapping(value = "/{urlname}/events", produces = "application/mf2+json")
  public ResponseEntity<MicroformatsResponseContainer> getEvents(
      @PathVariable("urlname") String urlname) throws MeetupApiException {
    MicroformatsResponseContainer<HFeed> responseContainer = new MicroformatsResponseContainer<>();
    responseContainer.items = new ArrayList<>();
    responseContainer.items.add(eventService.retrieveEvents(urlname));
    return new ResponseEntity<>(responseContainer, HttpStatus.OK);
  }
}
