package me.jvt.meetup.mf2.presentation.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class RootController {

  @GetMapping("/")
  public ModelAndView root() {
    return new ModelAndView(
        "redirect:https://www.jvt.me/posts/2019/08/31/microformats-meetup/?utm_medium=meetup-mf2");
  }
}
