package me.jvt.meetup.mf2.model.graphql;

public class RequestBody {
  private final String query;
  private final String variables;

  public RequestBody(String query, String variables) {
    this.query = query;
    this.variables = variables;
  }

  public String getQuery() {
    return query;
  }

  public String getVariables() {
    return variables;
  }
}
