package me.jvt.meetup.mf2.external.meetup.graphql;

import com.fasterxml.jackson.databind.node.ObjectNode;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.jvt.meetup.mf2.CacheConfiguration;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.model.graphql.RequestBody;
import me.jvt.meetup.mf2.restassured.RequestSpecificationFactory;
import org.springframework.cache.annotation.Cacheable;

public class MeetupClient {

  private static final String EVENT_PROPERTIES_PARTIAL_QUERY =
      "    title\n"
          + "    description\n"
          + "    dateTime\n"
          + "    endTime\n"
          + "    eventUrl\n"
          + "    group {\n"
          + "      name\n"
          + "      logo {\n"
          + "        id\n"
          + "        baseUrl\n"
          + "      }\n"
          + "      link\n"
          + "    }\n"
          + "    venue {\n"
          + "      id\n"
          + "      name\n"
          + "      address\n"
          + "      city\n"
          + "      postalCode\n"
          + "      country\n"
          + "    }\n";
  private static final String QUERY_EVENT =
      "query ($eventId: ID) {\n"
          + "  event(id: $eventId) {\n"
          + EVENT_PROPERTIES_PARTIAL_QUERY
          + "}"
          + "}";
  private static final String QUERY_EVENTS =
      "query ($urlname: String!) {\n"
          + "  groupByUrlname(urlname: $urlname) {\n"
          + "    name\n"
          + "    link\n"
          + "    unifiedEvents {\n"
          + "      edges {\n"
          + "        node {\n"
          + EVENT_PROPERTIES_PARTIAL_QUERY
          + "}"
          + "}"
          + "}"
          + "}"
          + "}";

  private final RequestSpecificationFactory factory;

  public MeetupClient(RequestSpecificationFactory factory) {
    this.factory = factory;
  }

  @Cacheable(CacheConfiguration.EVENTS_CACHE)
  public ObjectNode retrieveEvent(@SuppressWarnings("unused") String urlname, String id)
      throws MeetupApiException {
    RequestSpecification request = factory.newRequestSpecification();
    RequestBody body = new RequestBody(QUERY_EVENT, String.format("{\"eventId\": \"%s\"}", id));

    Response response =
        request.contentType(ContentType.JSON).body(body).post("https://api.meetup.com/gql");
    if (200 != response.getStatusCode()) {
      throw new MeetupApiException(
          "An error occurred when talking to the Meetup API", response.getStatusCode(), null);
    }
    return response.as(ObjectNode.class);
  }

  @Cacheable(CacheConfiguration.EVENTS_CACHE)
  public ObjectNode retrieveEvents(String urlname) throws MeetupApiException {
    RequestSpecification request = factory.newRequestSpecification();
    RequestBody body =
        new RequestBody(QUERY_EVENTS, String.format("{\"urlname\": \"%s\"}", urlname));

    Response response =
        request.contentType(ContentType.JSON).body(body).post("https://api.meetup.com/gql");
    if (200 != response.getStatusCode()) {
      throw new MeetupApiException(
          "An error occurred when talking to the Meetup API", response.getStatusCode(), null);
    }
    return response.as(ObjectNode.class);
  }
}
