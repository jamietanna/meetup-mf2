package me.jvt.meetup.mf2.service;

import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HFeed;
import org.springframework.stereotype.Service;

@Service
public interface EventService {

  HEvent retrieveEvent(String urlname, String id) throws MeetupApiException;

  HFeed retrieveEvents(String urlname) throws MeetupApiException;
}
