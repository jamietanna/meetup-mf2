package me.jvt.meetup.mf2.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;

public class HEvent {

  public String[] type;
  public Properties properties;

  public static class Properties {

    public String[] name;

    @JsonInclude(Include.NON_NULL)
    public Description[] description;

    public String[] start;
    public String[] end;
    public String[] url;

    @JsonInclude(Include.NON_NULL)
    public String[] location;

    @JsonInclude(Include.NON_NULL)
    public String[] published;

    @JsonInclude(Include.NON_NULL)
    public String[] updated;

    public List<Author> author;

    public static class Author {
      public String[] type;
      public AuthorProperties properties;
      public String value;

      public static class AuthorProperties {
        public String[] name;
        public String[] photo;
        public String[] url;
      }
    }

    public static class Description {
      public String html;
      public String value;
    }
  }
}
