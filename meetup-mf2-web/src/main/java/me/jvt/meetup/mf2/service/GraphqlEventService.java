package me.jvt.meetup.mf2.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.function.Consumer;
import me.jvt.meetup.mf2.converter.graphql.MeetupEventConverter;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.external.meetup.graphql.MeetupClient;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HFeed;

public class GraphqlEventService implements EventService {

  private final MeetupClient client;
  private final MeetupEventConverter converter;

  public GraphqlEventService(MeetupClient client, MeetupEventConverter converter) {
    this.client = client;
    this.converter = converter;
  }

  @Override
  public HEvent retrieveEvent(String urlname, String id) throws MeetupApiException {
    return converter.convert(client.retrieveEvent(urlname, id).get("data").get("event"));
  }

  @Override
  public HFeed retrieveEvents(String urlname) throws MeetupApiException {
    ObjectNode response = client.retrieveEvents(urlname);
    ObjectNode group = response.with("data").with("groupByUrlname");
    HFeed feed = new HFeed();
    feed.properties = new HFeed.Properties();
    feed.properties.name = arr(group.get("name").asText());
    feed.properties.url = arr(String.format("%s/events", group.get("link").asText()));
    feed.children = new ArrayList<>();
    group.with("unifiedEvents").withArray("edges").forEach(convertNode(feed));

    return feed;
  }

  private Consumer<JsonNode> convertNode(HFeed feed) {
    return n -> feed.children.add(converter.convert(n.get("node")));
  }

  private static String[] arr(String val) {
    return new String[] {val};
  }
}
