package me.jvt.meetup.mf2.converter.graphql;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import me.jvt.meetup.mf2.model.HEvent;

public class MeetupEventConverter {
  private static final String ONLINE_EVENT_VENUE_ID = "26906060";
  private static final String[] ONLINE_EVENT_VENUE = new String[] {"Online"};

  public HEvent convert(JsonNode node) {
    HEvent event = new HEvent();
    event.type = arr("h-event");
    HEvent.Properties properties = event.properties = new HEvent.Properties();
    properties.name = getTextArr(node, "title");
    properties.description = mapDescription(node);
    properties.start = getTextArr(node, "dateTime");
    properties.end = getTextArr(node, "endTime");
    properties.url = getTextArr(node, "eventUrl");
    properties.author = mapAuthor(node.get("group"));
    properties.location = mapLocation(node.get("venue"));

    return event;
  }

  private static String[] mapLocation(JsonNode venue) {
    if (venue.isNull() || ONLINE_EVENT_VENUE_ID.equals(venue.get("id").textValue())) {
      return ONLINE_EVENT_VENUE;
    }

    List<String> values = new ArrayList<>();
    values.add(getText(venue, "name"));
    values.add(getText(venue, "address"));
    values.add(getText(venue, "city"));
    values.add(getText(venue, "postalCode"));
    values.add(getText(venue, "country"));

    return arr(values.stream().filter(s -> !s.isEmpty()).collect(Collectors.joining(", ")));
  }

  private static String getText(JsonNode node, String property) {
    return node.get(property).textValue();
  }

  private static String[] getTextArr(JsonNode node, String property) {
    return arr(getText(node, property));
  }

  private static String[] arr(String v) {
    return new String[] {v};
  }

  private static HEvent.Properties.Description[] mapDescription(JsonNode node) {
    HEvent.Properties.Description description = new HEvent.Properties.Description();
    description.html = description.value = getText(node, "description");

    return new HEvent.Properties.Description[] {description};
  }

  private static List<HEvent.Properties.Author> mapAuthor(JsonNode group) {
    HEvent.Properties.Author author = new HEvent.Properties.Author();
    author.type = arr("h-card");
    author.properties = new HEvent.Properties.Author.AuthorProperties();
    author.value = getText(group, "name");
    author.properties.name = arr(author.value);
    author.properties.photo = arr(mapPhoto(group.get("logo")));
    author.properties.url = getTextArr(group, "link");

    return Collections.singletonList(author);
  }

  private static String mapPhoto(JsonNode logo) {
    // TODO: this does not actually work for all sizes of profile images
    return String.format("%s%s/600x600.webp", getText(logo, "baseUrl"), getText(logo, "id"));
  }
}
