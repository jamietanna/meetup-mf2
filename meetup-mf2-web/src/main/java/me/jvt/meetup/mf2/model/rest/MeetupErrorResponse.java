package me.jvt.meetup.mf2.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MeetupErrorResponse {

  public Error[] errors;

  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class Error {

    public String code;
    public String message;

    @JsonInclude(Include.NON_NULL)
    public String field;
  }
}
