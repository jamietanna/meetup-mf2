package me.jvt.meetup.mf2.service;

import java.util.ArrayList;
import java.util.List;
import me.jvt.meetup.mf2.converter.rest.MeetupEventConverter;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.external.meetup.rest.MeetupClient;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HFeed;
import me.jvt.meetup.mf2.model.HFeed.Properties;
import me.jvt.meetup.mf2.model.rest.MeetupEventResponse;
import me.jvt.meetup.mf2.model.rest.MeetupGroupResponse;

public class RestEventService implements EventService {

  private static final String MEETUP_EVENTS_URL_FORMAT_STRING = "https://www.meetup.com/%s/events";

  private final MeetupClient meetupClient;
  private final MeetupEventConverter meetupEventConverter;

  public RestEventService(MeetupClient meetupClient, MeetupEventConverter meetupEventConverter) {
    this.meetupClient = meetupClient;
    this.meetupEventConverter = meetupEventConverter;
  }

  public HEvent retrieveEvent(String urlname, String id) throws MeetupApiException {
    MeetupEventResponse meetupEventResponse = null;
    meetupEventResponse = meetupClient.retrieveEvent(urlname, id);
    return meetupEventConverter.convert(meetupEventResponse);
  }

  public HFeed retrieveEvents(String urlname) throws MeetupApiException {
    MeetupGroupResponse groupInformation = meetupClient.retrieveGroup(urlname);

    List<MeetupEventResponse> meetupEventsResponse = meetupClient.retrieveEvents(urlname);
    HFeed hFeed = new HFeed();
    hFeed.children = new ArrayList<>();
    for (MeetupEventResponse meetupEventResponse : meetupEventsResponse) {
      hFeed.children.add(meetupEventConverter.convert(meetupEventResponse));
    }

    hFeed.properties = new Properties();
    hFeed.properties.name = new String[] {groupInformation.name};
    hFeed.properties.url =
        new String[] {String.format(MEETUP_EVENTS_URL_FORMAT_STRING, groupInformation.urlname)};

    return hFeed;
  }
}
