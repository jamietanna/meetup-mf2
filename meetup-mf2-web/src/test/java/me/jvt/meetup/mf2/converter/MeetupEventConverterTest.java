package me.jvt.meetup.mf2.converter;

import static org.assertj.core.api.Assertions.assertThat;

import me.jvt.meetup.mf2.converter.rest.MeetupEventConverter;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HEvent.Properties.Author;
import me.jvt.meetup.mf2.model.rest.MeetupEventResponse;
import me.jvt.meetup.mf2.model.rest.MeetupEventResponse.Venue;
import me.jvt.meetup.mf2.model.rest.MeetupGroupResponse;
import me.jvt.meetup.mf2.model.rest.MeetupGroupResponse.KeyPhoto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MeetupEventConverterTest {

  private MeetupEventConverter sut;
  private MeetupEventResponse meetupEventResponse;

  @BeforeEach
  void setup() {
    meetupEventResponse = new MeetupEventResponse();
    meetupEventResponse.venue = new Venue();
    sut = new MeetupEventConverter();

    meetupEventResponse.description = "Description";
    meetupEventResponse.group = new MeetupGroupResponse();
    meetupEventResponse.group.name = "Something group";
    meetupEventResponse.group.urlname = "group-slug";
    meetupEventResponse.group.keyPhoto = new KeyPhoto();
    meetupEventResponse.group.keyPhoto.photoLink = "https://secure.meetupstatic.com/img.jpeg";
  }

  @Test
  void itConvertsStartDateTimeForUtc() {
    // given
    meetupEventResponse.time = 1567706400000L;
    meetupEventResponse.utcOffset = 0;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.start).containsExactly("2019-09-05T18:00:00Z");
  }

  @Test
  void itConvertsStartDateTimeForUtcPlusThree() {
    // given
    meetupEventResponse.time = 1567706400000L;
    meetupEventResponse.utcOffset = 3 * 3600000;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.start).containsExactly("2019-09-05T21:00:00+03:00");
  }

  @Test
  void itConvertsEndDateTimeForUtc() {
    // given
    meetupEventResponse.time = 1567706400000L;
    meetupEventResponse.utcOffset = 0;
    meetupEventResponse.duration = 7200000;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.end).containsExactly("2019-09-05T20:00:00Z");
  }

  @Test
  void itConvertsEndDateTimeForUtcMinuxFour() {
    // given
    meetupEventResponse.time = 1567706400000L;
    meetupEventResponse.utcOffset = -(4 * 3600000);
    meetupEventResponse.duration = 3600000;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.end).containsExactly("2019-09-05T15:00:00-04:00");
  }

  @Test
  void itExtractsTheName() {
    // given
    meetupEventResponse.name = "September event: Something good!";

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.name).containsExactly("September event: Something good!");
  }

  @Test
  void itExtractsTheDescription() {
    // given
    meetupEventResponse.description = "Some big <p>HTML</p> thing, maybe.";

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.description).hasSize(1);
    assertThat(actual.properties.description[0].html)
        .isEqualTo("Some big <p>HTML</p> thing, maybe.");
    assertThat(actual.properties.description[0].value).isEqualTo("Some big HTML thing, maybe.");
  }

  @Test
  void itHandlesNullDescription() {
    // given
    meetupEventResponse.description = null;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.description).isNull();
  }

  @Test
  void itExtractsTheUrl() {
    // given
    meetupEventResponse.link = "https://foo.bar.wibble/blah";

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.url).containsExactly("https://foo.bar.wibble/blah");
  }

  @Test
  void itIsAnHEvent() {
    // given

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.type).containsExactly("h-event");
  }

  @Test
  void itExtractsTheAddress() {
    // given
    meetupEventResponse.venue = new Venue();
    meetupEventResponse.venue.name = "Big Organisation";
    meetupEventResponse.venue.address1 = "123 some address, next street";
    meetupEventResponse.venue.city = "Cityville";
    meetupEventResponse.venue.localized_country_name = "Countrystan";

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.location).isNotEmpty();
    String[] location = actual.properties.location;
    assertThat(location)
        .containsExactly("Big Organisation, 123 some address, next street, Cityville, Countrystan");
  }

  @Test
  void itHandlesNullVenue() {
    // given
    meetupEventResponse.venue = null;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.location).isEqualTo(null);
  }

  @Test
  void itHandlesOnlineEventVenue() {
    // given
    meetupEventResponse.venue = new Venue();
    meetupEventResponse.venue.id =
        26906060; // via the Meetup API, this is the venue ID for all online vents

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.location).containsExactly("Online");
  }

  @Test
  void itExtractsThePublishDate() {
    // given
    meetupEventResponse.created = 1567706400000L;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.published).containsExactly("2019-09-05T18:00:00Z");
  }

  @Test
  void itExtractsTheUpdatedDate() {
    // given
    meetupEventResponse.updated = 1567706400000L;

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.updated).containsExactly("2019-09-05T18:00:00Z");
  }

  @Test
  void itSetsTheAuthor() {
    // given
    meetupEventResponse.group = new MeetupGroupResponse();
    meetupEventResponse.group.name = "Meetup group here";
    meetupEventResponse.group.urlname = "meetup-name";
    meetupEventResponse.group.keyPhoto = new KeyPhoto();
    meetupEventResponse.group.keyPhoto.photoLink = "https://secure.meetupstatic.com/img.png";

    // when
    HEvent actual = sut.convert(meetupEventResponse);

    // then
    assertThat(actual.properties.author).isNotNull();
    assertThat(actual.properties.author).hasSize(1);
    Author actualAuthor = actual.properties.author.get(0);
    assertThat(actualAuthor.type).containsExactly("h-card");
    assertThat(actualAuthor.value).isEqualTo("Meetup group here");
    assertThat(actualAuthor.properties.name).containsExactly("Meetup group here");
    assertThat(actualAuthor.properties.photo)
        .containsExactly("https://secure.meetupstatic.com/img.png");
    assertThat(actualAuthor.properties.url).containsExactly("https://www.meetup.com/meetup-name");
  }
}
