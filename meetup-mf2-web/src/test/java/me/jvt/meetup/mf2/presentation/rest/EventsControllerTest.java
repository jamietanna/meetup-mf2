package me.jvt.meetup.mf2.presentation.rest;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Collections;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HEvent.Properties;
import me.jvt.meetup.mf2.model.HEvent.Properties.Description;
import me.jvt.meetup.mf2.model.HFeed;
import me.jvt.meetup.mf2.model.MicroformatsResponseContainer;
import me.jvt.meetup.mf2.model.rest.MeetupErrorResponse;
import me.jvt.meetup.mf2.model.rest.MeetupErrorResponse.Error;
import me.jvt.meetup.mf2.service.EventService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(EventsController.class)
class EventsControllerTest {

  @Autowired private MockMvc mockMvc;

  @MockBean private EventService mockEventService;

  private HEvent fakeHevent;
  private HFeed fakeHfeed;
  private ObjectMapper objectMapper;

  @BeforeEach
  void setup() {
    objectMapper = new ObjectMapper();
    fakeHevent = new HEvent();
    fakeHfeed = new HFeed();
  }

  @Test
  void eventByIdReturnsMf2JsonContentType() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/event-name-here/events/12345"))
        // then
        .andExpect(
            MockMvcResultMatchers.header()
                .string("Content-Type", startsWith("application/mf2+json")));
  }

  @Test
  void receivesRequestForAGivenEventNameAndId() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/event-name-here/events/12345"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void receivesRequestForADifferentEventNameAndId() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/homebrew/events/345"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void serviceIsCalledToRetrieveEvent() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/homebrew-notts/events/1"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(mockEventService).retrieveEvent("homebrew-notts", "1");
  }

  @Test
  void serviceIsCalledToRetrieveEventAndReturnedInResponse() throws Exception {
    // given
    when(mockEventService.retrieveEvent(anyString(), anyString())).thenReturn(fakeHevent);
    fakeHevent.type = singletonArray("h-event");
    fakeHevent.properties = new Properties();
    fakeHevent.properties.description = new Description[1];
    fakeHevent.properties.description[0] = new Description();
    fakeHevent.properties.description[0].html = "an event that is happening";
    fakeHevent.properties.description[0].value = "an event that is happening";

    fakeHevent.properties.start = singletonArray("2019-09-18T17:30:00+0100");
    fakeHevent.properties.end = singletonArray("2019-09-18T19:30:00+0100");

    // when
    mockMvc
        .perform(get("/homebrew-notts/events/1"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.items").isArray())
        .andExpect(jsonPath("$.items").value(hasSize(1)))
        .andExpect(jsonPath("$.items[0].type").isArray())
        .andExpect(jsonPath("$.items[0].type").value(hasSize(1)))
        .andExpect(jsonPath("$.items[0].type[0]").value("h-event"))
        .andExpect(jsonPath("$.items[0].properties.start[0]").value("2019-09-18T17:30:00+0100"))
        .andExpect(jsonPath("$.items[0].properties.end[0]").value("2019-09-18T19:30:00+0100"));

    verify(mockEventService).retrieveEvent("homebrew-notts", "1");
  }

  @Test
  void meetupApiExceptionsAreMappedCorrectly() throws Exception {
    MeetupErrorResponse errorResponse = new MeetupErrorResponse();
    errorResponse.errors = new Error[2];
    errorResponse.errors[0] = new Error();
    errorResponse.errors[0].code = "error_foo";
    errorResponse.errors[0].message = "something more descriptive";
    errorResponse.errors[1] = new Error();
    errorResponse.errors[1].code = "error_validation";
    errorResponse.errors[1].message = "something was wrong!";
    errorResponse.errors[1].field = "id";

    MeetupApiException e = new MeetupApiException("Some error", 400, errorResponse);
    when(mockEventService.retrieveEvent(anyString(), anyString())).thenThrow(e);

    // when
    mockMvc
        .perform(get("/homebrew-notts/events/1"))
        // then
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(jsonPath("$.error_message").value("Some error"))
        .andExpect(jsonPath("$.errors").isArray())
        .andExpect(jsonPath("$.errors").value(hasSize(2)))
        .andExpect(jsonPath("$.errors[0].code").value("error_foo"))
        .andExpect(jsonPath("$.errors[0].message").value("something more descriptive"))
        .andExpect(jsonPath("$.errors[0].field").doesNotExist())
        .andExpect(jsonPath("$.errors[1].code").value("error_validation"))
        .andExpect(jsonPath("$.errors[1].message").value("something was wrong!"))
        .andExpect(jsonPath("$.errors[1].field").value("id"));
  }

  @Test
  void eventsReturnsMf2JsonContentType() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/event-name-here/events"))
        // then
        .andExpect(
            MockMvcResultMatchers.header()
                .string("Content-Type", startsWith("application/mf2+json")));
  }

  @Test
  void receivesRequestForAGivenEventName() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/event-name-here/events"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void receivesRequestForADifferentEventName() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/homebrew/events"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void serviceIsCalledToRetrieveEvents() throws Exception {
    // given

    // when
    mockMvc
        .perform(get("/homebrew-notts/events"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(mockEventService).retrieveEvents("homebrew-notts");
  }

  @Test
  void serviceIsCalledToRetrieveEventsAndReturnedInResponse() throws Exception {
    // given
    MicroformatsResponseContainer microformatsResponseContainer =
        new MicroformatsResponseContainer();
    when(mockEventService.retrieveEvents(anyString())).thenReturn(fakeHfeed);
    microformatsResponseContainer.items = Collections.singletonList(fakeHfeed);

    fakeHfeed.children = new ArrayList<>();
    fakeHfeed.children.add(fakeHevent);
    fakeHfeed.properties = new HFeed.Properties();
    fakeHfeed.properties.name = singletonArray("Homebrew Website Club: Nottingham");
    fakeHfeed.properties.url = singletonArray("https://www.meetup.com/homebrew-notts/events/");

    fakeHevent.type = singletonArray("h-event");
    fakeHevent.properties = new Properties();
    fakeHevent.properties.description = new Description[1];
    fakeHevent.properties.description[0] = new Description();
    fakeHevent.properties.description[0].html = "an event that is happening";
    fakeHevent.properties.description[0].value = "an event that is happening";
    fakeHevent.properties.start = singletonArray("2019-09-18T17:30:00+0100");
    fakeHevent.properties.end = singletonArray("2019-09-18T19:30:00+0100");

    // when
    mockMvc
        .perform(get("/homebrew-notts/events"))
        // then
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.items").isArray())
        .andExpect(jsonPath("$.items").value(hasSize(1)))
        .andExpect(jsonPath("$.items[0].type").isArray())
        .andExpect(jsonPath("$.items[0].type").value(hasSize(1)))
        .andExpect(jsonPath("$.items[0].type[0]").value("h-feed"))
        .andExpect(
            jsonPath("$.items[0].properties.name[0]").value("Homebrew Website Club: Nottingham"))
        .andExpect(
            jsonPath("$.items[0].properties.url[0]")
                .value("https://www.meetup.com/homebrew-notts/events/"))
        .andExpect(jsonPath("$.items[0].children").isArray())
        .andExpect(jsonPath("$.items[0].children").value(hasSize(1)))
        .andExpect(jsonPath("$.items[0].children[0].type").isArray())
        .andExpect(jsonPath("$.items[0].children[0].type").value(hasSize(1)))
        .andExpect(jsonPath("$.items[0].children[0].type[0]").value("h-event"))
        .andExpect(
            jsonPath("$.items[0].children[0].properties.start[0]")
                .value("2019-09-18T17:30:00+0100"))
        .andExpect(
            jsonPath("$.items[0].children[0].properties.end[0]").value("2019-09-18T19:30:00+0100"));
    // TODO: properties
    verify(mockEventService).retrieveEvents("homebrew-notts");
  }

  private String[] singletonArray(String value) {
    String[] strings = new String[1];
    strings[0] = value;
    return strings;
  }
}
