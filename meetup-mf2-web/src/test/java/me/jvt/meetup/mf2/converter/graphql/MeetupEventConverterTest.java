package me.jvt.meetup.mf2.converter.graphql;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.jvt.meetup.mf2.model.HEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class MeetupEventConverterTest {
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private HEvent event;

  private final MeetupEventConverter converter = new MeetupEventConverter();

  private static ObjectNode prepare() {
    ObjectNode node = OBJECT_MAPPER.createObjectNode();
    setText(node, "title", "#NottsTest - TBA");
    setText(
        node, "description", "Information about this meetup will appear here once available.\n\n");
    setText(node, "dateTime", "2021-02-03T19:00Z");
    setText(node, "endTime", "2021-02-03T20:30Z");
    setText(
        node, "eventUrl", "https://www.meetup.com/ministry-of-testing-nottingham/events/275558815");

    ObjectNode group = node.putObject("group");
    setText(group, "name", "MOT");
    setText(group, "link", "url");
    ObjectNode logo = group.putObject("logo");
    setText(logo, "id", "id");
    setText(logo, "baseUrl", "base");

    ObjectNode venue = node.putObject("venue");
    setText(venue, "id", "26906060");
    return node;
  }

  @Nested
  class Location {

    @Nested
    class WhenNull {
      @Test
      void itReturnsOnlineEvent() {
        ObjectNode node = prepare();
        node.putNull("venue");

        event = converter.convert(node);

        assertThat(event.properties.location).containsExactly("Online");
      }
    }

    @Nested
    class WhenOnline {
      @Test
      void ifOnlineEventIdItReturnsStaticValue() {
        ObjectNode node = prepare();
        ObjectNode venue = node.putObject("venue");
        setText(venue, "id", "26906060");
        setText(venue, "name", "ignored");

        event = converter.convert(node);

        assertThat(event.properties.location).containsExactly("Online");
      }
    }

    @Nested
    class WhenOffline {
      @Test
      void concatenatesAllFieldsPresent() {
        ObjectNode node = prepare();
        ObjectNode venue = node.putObject("venue");
        setText(venue, "id", "0");
        setText(venue, "name", "Venue name");
        setText(venue, "address", "123, Thing");
        setText(venue, "city", "Citysville");
        setText(venue, "postalCode", "XX1 1XX");
        setText(venue, "country", "UK");

        event = converter.convert(node);

        assertThat(event.properties.location)
            .containsExactly("Venue name, 123, Thing, Citysville, XX1 1XX, UK");
      }

      @Test
      void handlesMissingFields() {
        ObjectNode node = prepare();
        ObjectNode venue = node.putObject("venue");
        setText(venue, "id", "0");
        setText(venue, "name", "Venue name");
        setText(venue, "address", "");
        setText(venue, "city", "");
        setText(venue, "postalCode", "");
        setText(venue, "country", "");

        event = converter.convert(node);

        assertThat(event.properties.location).containsExactly("Venue name");
      }
    }
  }

  @Nested
  class Otherwise {
    @BeforeEach
    void setup() {
      ObjectNode node = prepare();

      event = converter.convert(node);
    }

    @Test
    void setsType() {
      assertThat(event.type).containsExactly("h-event");
    }

    @Test
    void mapsName() {
      assertThat(event.properties.name).containsExactly("#NottsTest - TBA");
    }

    @Test
    void mapsDescription() {
      HEvent.Properties.Description expected = new HEvent.Properties.Description();
      expected.html = "Information about this meetup will appear here once available.\n\n";
      expected.value = "Information about this meetup will appear here once available.\n\n";

      assertThat(event.properties.description)
          .usingRecursiveFieldByFieldElementComparator()
          .containsExactly(expected);
    }

    @Test
    void mapsStart() {
      assertThat(event.properties.start).containsExactly("2021-02-03T19:00Z");
    }

    @Test
    void mapsEnd() {
      assertThat(event.properties.end).containsExactly("2021-02-03T20:30Z");
    }

    @Test
    void mapsUrl() {
      assertThat(event.properties.url)
          .containsExactly(
              "https://www.meetup.com/ministry-of-testing-nottingham/events/275558815");
    }

    @Test
    void publishedIsNotSet() {
      assertThat(event.properties.published).isNull();
    }

    @Test
    void updatedIsNotSet() {
      assertThat(event.properties.updated).isNull();
    }

    @Test
    void hasOneAuthor() {
      assertThat(event.properties.author).hasSize(1);
    }

    @Nested
    class Author {
      private HEvent.Properties.Author author;

      @BeforeEach
      void setup() {
        author = event.properties.author.get(0);
      }

      @Test
      void mapsType() {
        assertThat(author.type).containsExactly("h-card");
      }

      @Test
      void mapsName() {
        assertThat(author.properties.name).containsExactly("MOT");
      }

      @Test
      void mapsPhoto() {
        assertThat(author.properties.photo).containsExactly("baseid/600x600.webp");
      }

      @Test
      void mapsUrl() {
        assertThat(author.properties.url).containsExactly("url");
      }

      @Test
      void mapsValue() {
        assertThat(author.value).isEqualTo("MOT");
      }
    }
  }

  private static void setText(ObjectNode node, String property, String text) {
    node.set(property, node.textNode(text));
  }
}
