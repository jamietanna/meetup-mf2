package me.jvt.meetup.mf2.external.meetup;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.jvt.meetup.mf2.Application;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.external.meetup.graphql.MeetupClient;
import me.jvt.meetup.mf2.restassured.RequestSpecificationFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = Application.class)
class MeetupClientIntegrationTest {
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Autowired private MeetupClient sut;

  private RequestSpecification mockRequestSpecification;
  private Response mockResponse;

  @MockBean private RequestSpecificationFactory mockRequestSpecificationFactory;

  @BeforeEach
  void setup() {
    mockRequestSpecification = mock(RequestSpecification.class, RETURNS_SELF);
    mockResponse = mock(Response.class);

    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);

    when(mockRequestSpecification.post(anyString())).thenReturn(mockResponse);
    when(mockResponse.getStatusCode()).thenReturn(200);
  }

  @Test
  void retrieveEventCachesWhenUsingSameParameters() throws MeetupApiException {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(prepare());
    sut.retrieveEvent("abc", "123");

    // when
    sut.retrieveEvent("abc", "123");

    // then
    verify(mockRequestSpecificationFactory, times(1)).newRequestSpecification();
  }

  @Test
  void retrieveEventDoesNotHitCacheWhenUsingDifferentParameters() throws MeetupApiException {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(prepare());
    sut.retrieveEvent("abc", "124");

    // when
    sut.retrieveEvent("abc", "125");

    // then
    verify(mockRequestSpecificationFactory, times(2)).newRequestSpecification();
  }

  @Test
  void retrieveEventsCachesWhenUsingSameParameter() throws MeetupApiException {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(prepare());
    sut.retrieveEvents("abc");

    // when
    sut.retrieveEvents("abc");

    // then
    verify(mockRequestSpecificationFactory, times(1)).newRequestSpecification();
  }

  @Test
  void retrieveEventsDoesNotHitCacheWhenUsingDifferentParameters() throws MeetupApiException {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(prepare());
    sut.retrieveEvents("123");

    // when
    sut.retrieveEvents("def");

    // then
    verify(mockRequestSpecificationFactory, times(2)).newRequestSpecification();
  }

  private static ObjectNode prepare() {
    ObjectNode node = OBJECT_MAPPER.createObjectNode();
    setText(node, "title", "#NottsTest - TBA");
    setText(
        node, "description", "Information about this meetup will appear here once available.\n\n");
    setText(node, "dateTime", "2021-02-03T19:00Z");
    setText(node, "endTime", "2021-02-03T20:30Z");
    setText(
        node, "eventUrl", "https://www.meetup.com/ministry-of-testing-nottingham/events/275558815");

    ObjectNode group = node.putObject("group");
    setText(group, "name", "MOT");
    setText(group, "link", "url");
    ObjectNode logo = group.putObject("logo");
    setText(logo, "id", "id");
    setText(logo, "baseUrl", "base");

    ObjectNode venue = node.putObject("venue");
    setText(venue, "id", "26906060");
    return node;
  }

  private static void setText(ObjectNode node, String property, String text) {
    node.set(property, node.textNode(text));
  }
}
