package me.jvt.meetup.mf2.service;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.jvt.meetup.mf2.converter.graphql.MeetupEventConverter;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.external.meetup.graphql.MeetupClient;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HFeed;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GraphqlEventServiceTest {
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @Mock private MeetupEventConverter converter;

  @Mock private MeetupClient client;

  @InjectMocks private GraphqlEventService service;

  @Nested
  class RetrieveEvent {

    @Nested
    class WhenPresent {

      @Mock private ObjectNode node;
      @Mock private ObjectNode data;
      @Mock private ObjectNode eventNode;
      @Mock private HEvent event;

      private HEvent actual;

      @BeforeEach
      void setup() throws MeetupApiException {
        when(client.retrieveEvent(any(), any())).thenReturn(node);
        when(node.get("data")).thenReturn(data);
        when(data.get("event")).thenReturn(eventNode);
        when(converter.convert(any())).thenReturn(event);

        actual = service.retrieveEvent("name", "1234");
      }

      @Test
      void delegatesToClient() throws MeetupApiException {
        verify(client).retrieveEvent("name", "1234");
      }

      @Test
      void delegatesEventDataToConverter() {
        verify(converter).convert(eventNode);
      }

      @Test
      void returnsConverted() {
        assertThat(actual).isEqualTo(event);
      }
    }

    @Nested
    class WhenError {
      @Test
      void exceptionIsBubbledUp(@Mock MeetupApiException e) throws MeetupApiException {
        when(client.retrieveEvent(any(), any())).thenThrow(e);

        assertThatThrownBy(() -> service.retrieveEvent(null, null)).isSameAs(e);
      }
    }
  }

  @Nested
  class RetrieveEvents {
    private final ObjectNode node = OBJECT_MAPPER.createObjectNode();

    private HFeed actual;

    @Nested
    class WhenPresent {
      @Mock private ObjectNode event0;
      @Mock private ObjectNode event1;

      @Mock private HEvent converted0;
      @Mock private HEvent converted1;

      @BeforeEach
      void setup() throws MeetupApiException {
        ObjectNode group = node.with("data").with("groupByUrlname");
        group.set("name", node.textNode("The Group Name"));
        group.set("link", node.textNode("https://from-meetup/"));
        ArrayNode edges = group.with("unifiedEvents").withArray("edges");
        edges.add(OBJECT_MAPPER.createObjectNode().set("node", event0));
        edges.add(OBJECT_MAPPER.createObjectNode().set("node", event1));

        when(client.retrieveEvents(any())).thenReturn(node);
        lenient().when(converter.convert(eq(event0))).thenReturn(converted0);
        lenient().when(converter.convert(eq(event1))).thenReturn(converted1);

        actual = service.retrieveEvents("pretty-name");
      }

      @Test
      void delegatesToClient() throws MeetupApiException {
        verify(client).retrieveEvents("pretty-name");
      }

      @Test
      void delegatesEventsToConverter() {
        verify(converter).convert(event0);
        verify(converter).convert(event1);
      }

      @Test
      void returnsConverted() {
        assertThat(actual.children).containsExactly(converted0, converted1);
      }

      @Test
      void hasHFeedType() {
        assertThat(actual.type).containsExactly("h-feed");
      }

      @Test
      void hasFeedName() {
        assertThat(actual.properties.name).containsExactly("The Group Name");
      }

      @Test
      void hasFeedUrl() {
        assertThat(actual.properties.url).containsExactly("https://from-meetup//events");
      }
    }

    @Nested
    class WhenEmpty {

      @BeforeEach
      void setup() throws MeetupApiException {
        ObjectNode group = node.with("data").with("groupByUrlname");
        group.set("name", node.textNode("No Upcoming Events Group"));
        group.set("link", node.textNode("https://from-meetup/"));
        group.with("unifiedEvents").withArray("edges");

        when(client.retrieveEvents(any())).thenReturn(node);

        actual = service.retrieveEvents("pretty-name");
      }

      @Test
      void returnsNoChildren() {
        assertThat(actual.children).isEmpty();
      }

      @Test
      void hasHFeedType() {
        assertThat(actual.type).containsExactly("h-feed");
      }

      @Test
      void hasFeedName() {
        assertThat(actual.properties.name).containsExactly("No Upcoming Events Group");
      }

      @Test
      void hasFeedUrl() {
        assertThat(actual.properties.url).containsExactly("https://from-meetup//events");
      }
    }

    @Nested
    class WhenError {
      @Test
      void exceptionIsBubbledUp(@Mock MeetupApiException e) throws MeetupApiException {
        when(client.retrieveEvents(any())).thenThrow(e);

        assertThatThrownBy(() -> service.retrieveEvents(null)).isSameAs(e);
      }
    }
  }
}
