package me.jvt.meetup.mf2.external.meetup;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.List;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.external.meetup.rest.MeetupClient;
import me.jvt.meetup.mf2.model.rest.MeetupErrorResponse;
import me.jvt.meetup.mf2.model.rest.MeetupEventResponse;
import me.jvt.meetup.mf2.model.rest.MeetupGroupResponse;
import me.jvt.meetup.mf2.restassured.RequestSpecificationFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
class MeetupClientTest {

  @Mock private MeetupErrorResponse mockMeetupErrorResponse;
  @Mock private MeetupEventResponse mockMeetupEventResponse;
  @Mock private MeetupGroupResponse mockMeetupGroupResponse;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification mockRequestSpecification;

  @Mock private RequestSpecificationFactory mockRequestSpecificationFactory;
  @Mock private Response mockResponse;

  private MeetupClient sut;

  @BeforeEach
  void setup() {
    when(mockRequestSpecificationFactory.newRequestSpecification())
        .thenReturn(mockRequestSpecification);

    when(mockRequestSpecification.get(anyString())).thenReturn(mockResponse);
    lenient().when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupEventResponse);

    sut = new MeetupClient(mockRequestSpecificationFactory);
  }

  @Test
  void retrieveEventReturnsMeetupResponseWhenSuccessful() throws MeetupApiException {
    // given
    when(mockResponse.getStatusCode()).thenReturn(200);

    // when
    MeetupEventResponse actual = sut.retrieveEvent("test-me", "1234");

    // then
    assertThat(actual).isSameAs(mockMeetupEventResponse);
    verify(mockRequestSpecification).accept("application/json");

    verify(mockRequestSpecification).baseUri("https://api.meetup.com");
    verify(mockRequestSpecification).get("/test-me/events/1234");
    verify(mockRequestSpecification).queryParam("fields", "group_key_photo");

    verify(mockResponse).as(MeetupEventResponse.class);
  }

  @Test
  void retrieveEventThrowsErrorWhen400FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(400);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvent("test-me", "12345");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventThrowsErrorWhen403FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(403);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvent("test-me", "12345");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventThrowsErrorWhen404FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(404);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvent("test-me", "12345");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventThrowsErrorWhen410FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(410);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvent("test-me", "12345");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.GONE);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventThrowsErrorWhen429FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(429);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvent("test-me", "12345");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.TOO_MANY_REQUESTS);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventThrowsErrorWhen500FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(500);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvent("test-me", "12345");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventsReturnsMeetupResponseWhenSuccessful() throws MeetupApiException {
    // given
    when(mockResponse.as(any(Class.class)))
        .thenReturn(new MeetupEventResponse[] {mockMeetupEventResponse});
    when(mockResponse.getStatusCode()).thenReturn(200);

    // when
    List<MeetupEventResponse> actual = sut.retrieveEvents("test-me");

    // then
    assertThat(actual).containsExactly(mockMeetupEventResponse);
    verify(mockRequestSpecification).accept("application/json");
    verify(mockRequestSpecification).queryParam("fields", "group_key_photo");
    verify(mockRequestSpecification).queryParam("state", "upcoming");

    verify(mockRequestSpecification).baseUri("https://api.meetup.com");
    verify(mockRequestSpecification).get("/test-me/events");

    verify(mockResponse).as(MeetupEventResponse[].class);
  }

  @Test
  void retrieveEventsThrowsErrorWhen400FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(400);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvents("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventsThrowsErrorWhen403FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(403);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvents("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventsThrowsErrorWhen404FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(404);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvents("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventsThrowsErrorWhen410FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(410);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvents("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.GONE);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventsThrowsErrorWhen429FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(429);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvents("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.TOO_MANY_REQUESTS);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveEventsThrowsErrorWhen500FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(500);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveEvents("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveGroupReturnsMeetupResponseWhenSuccessful() throws MeetupApiException {
    // given
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupGroupResponse);
    when(mockResponse.getStatusCode()).thenReturn(200);

    // when
    MeetupGroupResponse actual = sut.retrieveGroup("test-me");

    // then
    assertThat(actual).isEqualTo(mockMeetupGroupResponse);
    verify(mockRequestSpecification).accept("application/json");

    verify(mockRequestSpecification).baseUri("https://api.meetup.com");
    verify(mockRequestSpecification).get("/test-me");

    verify(mockResponse).as(MeetupGroupResponse.class);
  }

  @Test
  void retrieveGroupThrowsErrorWhen400FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(400);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveGroup("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveGroupThrowsErrorWhen403FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(403);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveGroup("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveGroupThrowsErrorWhen404FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(404);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveGroup("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveGroupThrowsErrorWhen410FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(410);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveGroup("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.GONE);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveGroupThrowsErrorWhen429FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(429);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveGroup("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.TOO_MANY_REQUESTS);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }

  @Test
  void retrieveGroupThrowsErrorWhen500FromApi() {
    // given
    when(mockResponse.getStatusCode()).thenReturn(500);
    when(mockResponse.as(any(Class.class))).thenReturn(mockMeetupErrorResponse);

    // when
    MeetupApiException actual = null;
    try {
      sut.retrieveGroup("test-me");
    } catch (MeetupApiException e) {
      actual = e;
    }

    // then
    assertThat(actual).isNotNull();
    assertThat(actual).hasMessage("An error occurred when talking to the Meetup API");
    assertThat(actual.getMeetupErrorResponse()).isSameAs(mockMeetupErrorResponse);
    assertThat(actual.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    verify(mockResponse).as(MeetupErrorResponse.class);
  }
}
