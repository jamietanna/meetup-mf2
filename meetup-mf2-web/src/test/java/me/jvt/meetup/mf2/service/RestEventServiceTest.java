package me.jvt.meetup.mf2.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import me.jvt.meetup.mf2.converter.rest.MeetupEventConverter;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.external.meetup.rest.MeetupClient;
import me.jvt.meetup.mf2.model.HEvent;
import me.jvt.meetup.mf2.model.HFeed;
import me.jvt.meetup.mf2.model.rest.MeetupEventResponse;
import me.jvt.meetup.mf2.model.rest.MeetupGroupResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RestEventServiceTest {

  @Mock private HEvent mockHevent;

  @Mock private MeetupClient mockMeetupClient;

  @Mock private MeetupEventConverter mockMeetupEventConverter;

  private MeetupGroupResponse fakeMeetupGroupResponse;
  private MeetupEventResponse mockMeetupEventResponse;

  private RestEventService sut;

  @BeforeEach
  void setup() throws MeetupApiException {
    fakeMeetupGroupResponse = new MeetupGroupResponse();
    mockMeetupEventResponse = new MeetupEventResponse();

    sut = new RestEventService(mockMeetupClient, mockMeetupEventConverter);

    lenient().when(mockMeetupClient.retrieveGroup(anyString())).thenReturn(fakeMeetupGroupResponse);
    fakeMeetupGroupResponse.name = "Foo bar group";
    fakeMeetupGroupResponse.urlname = "something-url-name";
  }

  @Test
  void retrieveEventDelegatesToMeetupClientAndReturnsConvertedEvent() throws MeetupApiException {
    // given
    when(mockMeetupClient.retrieveEvent(anyString(), anyString()))
        .thenReturn(mockMeetupEventResponse);
    when(mockMeetupEventConverter.convert(any(MeetupEventResponse.class))).thenReturn(mockHevent);

    // when
    HEvent actual = sut.retrieveEvent("wibble", "foo");

    // then
    assertThat(actual).isSameAs(mockHevent);
    verify(mockMeetupClient).retrieveEvent("wibble", "foo");
    verify(mockMeetupEventConverter).convert(mockMeetupEventResponse);
  }

  @Test
  void retrieveEventDoesNotHandleMeetupApiException() throws MeetupApiException {
    // given
    MeetupApiException meetupApiException = new MeetupApiException("Oh no!", 500, null);
    when(mockMeetupClient.retrieveEvent(anyString(), anyString())).thenThrow(meetupApiException);

    // when
    assertThatThrownBy(
            () -> {
              sut.retrieveEvent("wibble", "foo");
            })
        // then
        .isSameAs(meetupApiException);
  }

  @Test
  void retrieveEventsDelegatesToMeetupClientAndReturnsConvertedEvents() throws MeetupApiException {
    // given
    MeetupEventResponse mockMeetupEventResponse1 = mock(MeetupEventResponse.class);
    MeetupEventResponse mockMeetupEventResponse2 = mock(MeetupEventResponse.class);
    List<MeetupEventResponse> events = new ArrayList<>();
    events.add(mockMeetupEventResponse);
    events.add(mockMeetupEventResponse1);
    events.add(mockMeetupEventResponse2);

    HEvent mockHevent1 = mock(HEvent.class);
    HEvent mockHevent2 = mock(HEvent.class);

    when(mockMeetupClient.retrieveEvents(anyString())).thenReturn(events);
    when(mockMeetupEventConverter.convert(mockMeetupEventResponse)).thenReturn(mockHevent);
    when(mockMeetupEventConverter.convert(mockMeetupEventResponse1)).thenReturn(mockHevent1);
    when(mockMeetupEventConverter.convert(mockMeetupEventResponse2)).thenReturn(mockHevent2);

    // when
    HFeed actual = sut.retrieveEvents("wibble");

    // then
    assertThat(actual.children).hasSize(3);
    assertThat(actual.children).containsExactly(mockHevent, mockHevent1, mockHevent2);

    verify(mockMeetupClient).retrieveEvents("wibble");
    verify(mockMeetupEventConverter).convert(mockMeetupEventResponse);
    verify(mockMeetupEventConverter).convert(mockMeetupEventResponse1);
    verify(mockMeetupEventConverter).convert(mockMeetupEventResponse2);
  }

  @Test
  void retrieveEventsSetsHfeedPropertiesFromMeetupClientGroupApiCall() throws MeetupApiException {
    // given
    List<MeetupEventResponse> events = new ArrayList<>();
    events.add(mockMeetupEventResponse);

    when(mockMeetupClient.retrieveEvents(anyString())).thenReturn(events);
    when(mockMeetupEventConverter.convert(mockMeetupEventResponse)).thenReturn(mockHevent);

    // when
    HFeed actual = sut.retrieveEvents("wibble");

    // then
    assertThat(actual.properties.name).containsExactly("Foo bar group");
    assertThat(actual.properties.url)
        .containsExactly("https://www.meetup.com/something-url-name/events");

    verify(mockMeetupClient).retrieveEvents("wibble");
    verify(mockMeetupEventConverter).convert(mockMeetupEventResponse);
    verify(mockMeetupClient).retrieveGroup("wibble");
  }

  @Test
  void retrieveEventsDelegatesToMeetupClientAndReturnsEmptyWhenNoEvents()
      throws MeetupApiException {
    // given
    when(mockMeetupClient.retrieveEvents(anyString())).thenReturn(Collections.emptyList());

    // when
    HFeed actual = sut.retrieveEvents("wibble");

    // then
    assertThat(actual.children).isEmpty();
  }

  @Test
  void retrieveEventsStillReturnsGroupInformationWhenNoEvents() throws MeetupApiException {
    // given
    when(mockMeetupClient.retrieveEvents(anyString())).thenReturn(Collections.emptyList());

    // when
    HFeed actual = sut.retrieveEvents("wibble");

    // then
    assertThat(actual.properties.name).containsExactly("Foo bar group");
    assertThat(actual.properties.url)
        .containsExactly("https://www.meetup.com/something-url-name/events");
  }

  @Test
  void retrieveEventsDoesNotHandleMeetupApiException() throws MeetupApiException {
    // given
    MeetupApiException meetupApiException = new MeetupApiException("Oh no!", 500, null);
    when(mockMeetupClient.retrieveEvents(anyString())).thenThrow(meetupApiException);

    // when
    assertThatThrownBy(
            () -> {
              sut.retrieveEvents("wibble");
            })
        // then
        .isSameAs(meetupApiException);
  }
}
