package me.jvt.meetup.mf2.external.meetup.graphql;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.node.ObjectNode;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.jvt.meetup.mf2.exception.MeetupApiException;
import me.jvt.meetup.mf2.model.graphql.RequestBody;
import me.jvt.meetup.mf2.restassured.RequestSpecificationFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
class MeetupClientTest {

  @Mock private RequestSpecificationFactory factory;

  @Mock(answer = Answers.RETURNS_SELF)
  private RequestSpecification requestSpecification;

  @Mock private ObjectNode node;
  @Mock private Response response;
  @Captor private ArgumentCaptor<RequestBody> requestCaptor;

  @InjectMocks private MeetupClient client;

  private ObjectNode actual;

  @Nested
  class RetrieveEvent {
    @Nested
    class HappyPath {

      @BeforeEach
      void setup() throws MeetupApiException {
        when(factory.newRequestSpecification()).thenReturn(requestSpecification);
        when(requestSpecification.post(anyString())).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.as(any(Class.class))).thenReturn(node);

        actual = client.retrieveEvent(null, "id");
      }

      @Test
      void postsToUrl() {
        verify(requestSpecification).post("https://api.meetup.com/gql");
      }

      @Test
      void sendsContentTypeJson() {
        verify(requestSpecification).contentType(ContentType.JSON);
      }

      @Test
      void setsQueryInBody() {
        verify(requestSpecification).body(requestCaptor.capture());

        assertThat(requestCaptor.getValue().getQuery())
            .isEqualToIgnoringWhitespace(
                "query ($eventId: ID) {\n"
                    + "  event(id: $eventId) {\n"
                    + "    title\n"
                    + "    description\n"
                    + "    dateTime\n"
                    + "    endTime\n"
                    + "    eventUrl\n"
                    + "    group {\n"
                    + "      name\n"
                    + "      logo {\n"
                    + "        id\n"
                    + "        baseUrl\n"
                    + "      }\n"
                    + "      link\n"
                    + "    }\n"
                    + "    venue {\n"
                    + "      id\n"
                    + "      name\n"
                    + "      address\n"
                    + "      city\n"
                    + "      postalCode\n"
                    + "      country\n"
                    + "    }\n"
                    + "  }\n"
                    + "}\n"
                    + "\n");
      }

      @Test
      void setsVariablesInBody() {
        verify(requestSpecification).body(requestCaptor.capture());

        assertThat(requestCaptor.getValue().getVariables())
            .isEqualToIgnoringWhitespace("{\"eventId\": \"id\"}");
      }

      @Test
      void returnsObjectNode() {
        assertThat(actual).isEqualTo(node);
      }
    }

    @Nested
    class WhenNot200Ok {
      private MeetupApiException thrown;

      @BeforeEach
      void setup() {
        when(factory.newRequestSpecification()).thenReturn(requestSpecification);
        when(requestSpecification.post(anyString())).thenReturn(response);
        when(response.getStatusCode()).thenReturn(500);

        thrown =
            catchThrowableOfType(() -> client.retrieveEvent(null, null), MeetupApiException.class);
      }

      @Test
      void itHasGenericMessage() {
        assertThat(thrown).hasMessage("An error occurred when talking to the Meetup API");
      }

      @Test
      void itHasStatusCode() {
        assertThat(thrown.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
      }

      @Test
      void itDoesNotHaveErrorResponse() {
        assertThat(thrown.getMeetupErrorResponse()).isNull();
      }
    }
  }

  @Nested
  class RetrieveEvents {
    @Nested
    class HappyPath {

      @BeforeEach
      void setup() throws MeetupApiException {
        when(factory.newRequestSpecification()).thenReturn(requestSpecification);
        when(requestSpecification.post(anyString())).thenReturn(response);
        when(response.getStatusCode()).thenReturn(200);
        when(response.as(any(Class.class))).thenReturn(node);

        actual = client.retrieveEvents("the-pretty-name");
      }

      @Test
      void postsToUrl() {
        verify(requestSpecification).post("https://api.meetup.com/gql");
      }

      @Test
      void sendsContentTypeJson() {
        verify(requestSpecification).contentType(ContentType.JSON);
      }

      @Test
      void setsQueryInBody() {
        verify(requestSpecification).body(requestCaptor.capture());

        assertThat(requestCaptor.getValue().getQuery())
            .isEqualToIgnoringWhitespace(
                "query ($urlname: String!) {\n"
                    + "  groupByUrlname(urlname: $urlname) {\n"
                    + "    name\n"
                    + "    link\n"
                    + "    unifiedEvents {\n"
                    + "      edges {\n"
                    + "        node {"
                    + ""
                    + "    title\n"
                    + "    description\n"
                    + "    dateTime\n"
                    + "    endTime\n"
                    + "    eventUrl\n"
                    + "    group {\n"
                    + "      name\n"
                    + "      logo {\n"
                    + "        id\n"
                    + "        baseUrl\n"
                    + "      }\n"
                    + "      link\n"
                    + "    }\n"
                    + "    venue {\n"
                    + "      id\n"
                    + "      name\n"
                    + "      address\n"
                    + "      city\n"
                    + "      postalCode\n"
                    + "      country\n"
                    + "    }\n"
                    + "  }\n"
                    + "}\n"
                    + "}\n"
                    + "}\n"
                    + "}\n"
                    + "\n");
      }

      @Test
      void setsVariablesInBody() {
        verify(requestSpecification).body(requestCaptor.capture());

        assertThat(requestCaptor.getValue().getVariables())
            .isEqualToIgnoringWhitespace("{\"urlname\": \"the-pretty-name\"}");
      }

      @Test
      void returnsObjectNode() {
        assertThat(actual).isEqualTo(node);
      }
    }

    @Nested
    class WhenNot200Ok {
      private MeetupApiException thrown;

      @BeforeEach
      void setup() {
        when(factory.newRequestSpecification()).thenReturn(requestSpecification);
        when(requestSpecification.post(anyString())).thenReturn(response);
        when(response.getStatusCode()).thenReturn(500);

        thrown =
            catchThrowableOfType(() -> client.retrieveEvent(null, null), MeetupApiException.class);
      }

      @Test
      void itHasGenericMessage() {
        assertThat(thrown).hasMessage("An error occurred when talking to the Meetup API");
      }

      @Test
      void itHasStatusCode() {
        assertThat(thrown.getMeetupErrorStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
      }

      @Test
      void itDoesNotHaveErrorResponse() {
        assertThat(thrown.getMeetupErrorResponse()).isNull();
      }
    }
  }
}
